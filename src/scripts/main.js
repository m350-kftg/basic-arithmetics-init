function calculate(e) {
  // Verhindern, dass Webseite nach Eingabe neu geladen wird
  e.preventDefault();
  // Eingegebene Zahlen auslesen und in Integer umwandeln
  const elNr1 = document.getElementById('nr-1');
  const elNr2 = document.getElementById('nr-2');
  const nr1 = parseInt(elNr1.value);
  const nr2 = parseInt(elNr2.value);

  // Nur rechnen, wenn gültige Zahlen eingegeben wurden
  if (nr1 && nr2) {
    // Selektierte Grundrechenart ermitteln
    const elSelOp = document.querySelector('#operation-input input:checked');
    const selOp = elSelOp.id;
    // Resultat ermitteln
    let result = 'Resultat der Rechnung ' + nr1;
    if (selOp === 'add') {
      result += '+' + nr2 + ': ' + (nr1 + nr2);
    } else if (selOp === 'sub') {
      result += '-' + nr2 + ': ' + (nr1 - nr2);
    } else if (selOp === 'mult') {
      result += '*' + nr2 + ': ' + nr1 * nr2;
    } else {
      result += '/' + nr2 + ': ' + nr1 / nr2;
    }
    // Resultat als Inhalt in Ausgabe Element schreiben
    const elResult = document.getElementById('result');
    elResult.textContent = result;
  }
  // Eingaben löschen
  elNr1.value = '';
  elNr2.value = '';
}

// Button an Klick Event binden
const elCalcBtn = document.getElementById('calculate');
elCalcBtn.addEventListener('click', calculate);
